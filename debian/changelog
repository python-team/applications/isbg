isbg (2.3.1-5) unstable; urgency=medium

  * Team upload.
  * Fix build with Sphinx 8 (Closes: #1090236)

 -- Alexandre Detiste <tchet@debian.org>  Wed, 25 Dec 2024 21:31:34 +0100

isbg (2.3.1-4) unstable; urgency=medium

  * Team upload.
  * Drop extra TypeVar, fails in Python 3.12 (Closes: #1056415)
  * Switch to dh-sequences (Closes: #1047959)
  * Bump policy version (no changes)
  * Switch to autopkgtest-pkg-pybuild
  * Drop unused Depends
  * Drop salsa-ci.yml

 -- Jochen Sprickerhof <jspricke@debian.org>  Tue, 02 Jan 2024 11:39:13 +0100

isbg (2.3.1-3) unstable; urgency=medium

  [ Debian Janitor ]
  * Set upstream metadata fields: Repository.
  * Remove obsolete fields Contact, Name from debian/upstream/metadata (already
    present in machine-readable debian/copyright).

 -- Jelmer Vernooĳ <jelmer@debian.org>  Wed, 25 May 2022 21:46:21 +0100

isbg (2.3.1-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

 -- Sandro Tosi <morph@debian.org>  Wed, 15 Sep 2021 00:16:15 -0400

isbg (2.3.1-1) unstable; urgency=medium

  * New upstream version.
  * New uploader. Taowa Munene-Tardif will be taking over this package.
  * d/control: use debhelper 13.
  * d/tests: set XDG_CACHE_HOME explicitly.

 -- Louis-Philippe Véronneau <pollo@debian.org>  Mon, 17 Aug 2020 17:44:03 -0400

isbg (2.3.0-1) unstable; urgency=medium

  * New upstream version. (Closes: #944520)
  * Update Standards-Version to 4.5.0.0 (no changes needed).
  * Fix failing testsuite (and autopkgtests). (Closes: #946145)
  * d/patches: refresh 0001.
  * d/control: add a dependency on sphinxdoc.
  * d/source/lintian-overrides: delete unused overrides.
  * d/gitlab-ci.yml: rename to salsa-ci.yml.

 -- Louis-Philippe Véronneau <pollo@debian.org>  Sat, 08 Feb 2020 14:29:40 -0500

isbg (2.2.1-2) unstable; urgency=medium

  * Upstream VCS migrated to gitlab.com.

 -- Louis-Philippe Véronneau <pollo@debian.org>  Mon, 18 Nov 2019 16:22:40 -0500

isbg (2.2.1-1) unstable; urgency=medium

  * New upstream release. (Closes: #944520)
  * d/isbg.examples: Package examples.
  * d/tests: use upstream unit tests for autopkgtests.

 -- Louis-Philippe Véronneau <pollo@debian.org>  Sun, 17 Nov 2019 11:01:17 -0500

isbg (2.1.5-1) unstable; urgency=medium

  * Initial release. (Closes: #587458)

 -- Louis-Philippe Véronneau <pollo@debian.org>  Sun, 20 Oct 2019 04:00:29 -0400
